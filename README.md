# Test Case:
**Board**: Adafruit Feather M0   
**SD Card:** MS-DOS (FAT32)
Hardware setup - default SPI bus, but the SD CS is on hardware pin 8 (A2) (to prevent my example conflicting with the onboard ATWINC1500)

1. Upload the SDUBoot project to Feather via USB. Connect SD card to SPI bus and CS to hardware pin 8 (A2)
2. Format SD card to MS-DOS FAT32
3. Copy `FIRMWARE.bin` from `Blink_ArduinoIDE` and insert SD card
4. Press reset on board and the LED should flash 250ms on/250ms off
5. Remove SD card and copy `FIRMWARE.bin` from `Blink_PIO`
6. Press reset on board and see how LED speed doesn’t change (LED should flash 1s on/1s off) and how file doesnt get deleted from SD card
7. Copy `FIRMWARE.bin` from `Fade_ArduinoIDE` and insert SD card
8. Press reset on board and the LED should fade  on and off slower than the boot loader fade
